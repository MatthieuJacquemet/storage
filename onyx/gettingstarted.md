# Getting access

Prerequisite:
* a Gitlab account
* openssh

install openssh

    $ sudo apt install openssh

## Create a RSA key pair

For convenience and security, whe will use SSH authentificationd, this way we don't have to type in our passward each time we push to gitlab.

First: verify we don't already have one. If so, skip the next step.

    $ ls ~/.ssh || mkdir ~/.ssh;

To create a new RSA key paire issue the command:

    $ ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ""

## Register you public key on Gitlab

First copy your public key:

    # copy the output of this command
    $ cat ~/.ssh/id_rsa.pub

    # if you have installed xclip you can do
    $ xclip -sel clip < ~/.ssh/id_rsa.pub

Then paste if to your Gitlab account settings:

1. Click your avatar in the upper right corner and select `Settings`.
2. Navigate to `SSH Keys` and past your public key in the `Key` field.
2. Click the `Add key` button.

Test that everything works fine: 

    # it should output a welcome message
    $ ssh -T git@gitlab.com


## Request Access

Simply click the button as shown bellow and wait your access to be granted 

![request access](images/requestaccess.png)

## Getting started

Once you have access you can simply clone the ripository

    $ git clone git@gitlab.com:MatthieuJacquemet/onyx.git

Now you are ready to bring your modifications to the distribution !