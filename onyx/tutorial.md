# Getting involved

Prerequisite :
* git
* openssh
* sshfs
* python3
* python3-pip


## Install the required components

### Install the dependencies

    $ sudo apt install git
    $ sudo apt install openssh
    $ sudo apt install sshfs
    $ sudo apt install python3
    $ sudo apt install python3-pip

### Install QEMU/KVM and its tools

First make sure your CPU has Intel VT or AMD-V Virtualization extensions.
In some systems, this is disabled on BIOS and you may need to enable it.

    $ cat /proc/cpuinfo | egrep "vmx|svm" --color

if its finds something you're ok, if it doesn't the virtualisation will be slower

    $ sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager ovmf

After the installation, check if the Kernel module is loaded

    $ lsmod | grep kvm
    kvm_intel             245760  0
    kvmgt                  28672  0
    mdev                   24576  2 kvmgt,vfio_mdev
    vfio                   32768  3 kvmgt,vfio_mdev,vfio_iommu_type1
    kvm                   634880  2 kvmgt,kvm_intel
    irqbypass              16384  1 kvm

Start the libvirt daemon

    $ sudo systemctl start libvirtd

Make it start at boot

    $ sudo systemctl enable libvirtd

### Download ArchLinux install iso

Click [here](https://www.archlinux.org/download/)
to download the installation iso.

Place the iso in a apropriate folder,
for this tutorial i will use `~/ISO/` folder

## Setup the VM on virt-manager

Open `virt-manager` and create a new vm

![](images/tutorial1.png)

Select `ISO image install CD` and click next

![](.images/tutorial2.png)

![](images/tutorial3.png)

Add a new pool and name it for instance `ISO`

![](images/tutorial4.png)

![](images/tutorial5.png)

Type in the path we have downloaded our arch iso
"the `ISO` folder in your <strong>home directory</strong>"

![](images/tutorial6.png)

Select to <strong>ISO</strong> pool and then 
select the <strong>archlinux</strong> iso.

![](images/tutorial7.png)

![](images/tutorial8.png)

Allocate at least `4000` Mb if your system has
<strong>8Gb</strong> and `12000` Mb if you have <strong>16Gb</strong>.
Set the `CPU` count at the maximum, eg: 4 if you have a 4 core CPU.

![](images/tutorial9.png)

As for now set about <strong>15Gb</strong> of storage for our VM.

![](images/tutorial10.png)

Name your vm <strong>ONYX</strong> and d'ont forget
to check `Customize the configuration before install`

![](images/tutorial11.png)

Check that `Q35` <strong>chipset</strong> is selected
and `OVMF_CODE.fd` <strong>microprogram</strong> is selected.
Don't forget to hit `Apply` each time you make a modification.

![](images/tutorial12.png)

Select the vm <strong>storage</strong> in the left pan
set the <strong>Bus</strong> to `SATA`.

![](images/tutorial13.png)

In `Boot option` : check the `CD disk` to enable it and bring it to the top.
Finally click `Start installation`.

![](images/tutorial14.png)

Now your vm should appear in the vm list. open it.

![](images/tutorial15.png)

Click `start` button and once you see boot menu
press enter to boot `Arch Linux archiso x86_64 UEFI CD`

![](images/tutorial16.png)

## Build your distribution

Now you have a working vm, you should be automatically logged in as root.
All the following commands will be <strong>executed on the vm</strong>.

If you want to change the keyboard layout:

    # for AZERTY layout use fr
    $ localectl set-keymap --no-convert fr

### Setup the VM storage

Check the sata disk for our storage is present.

    $ lsblk
    # you sould have a sda disk present
    # note that this is the virtual disk (ONYX.qcow2) we have created

Use a partition utility to create the partitions for our vm.
You can use fdisk, parted... , we will use cfdisk.

    $ cfdisk

![](images/tutorial17.png)

1. Create a `New` partition <strong>(sda1)</strong> of `10G` for the system.
2. Create another partition <strong>(sda2)</strong> of `512M` for the <strong>EFI</strong>.
3. Go to `Type` and select `EFI file system` on top the list.
4. Create a third partition <strong>(sda3)</strong> for the `swap` of the same size as the vm <strong>memory</strong>.
5. Same as before, set its `Type` to `linux swap`.
6. Create the last partition <strong>(sda4)</strong> for the <strong>home</strong> with the free space left.
7. Finally `Write` to make your modifications take effect and `Quit`.

Now we will create the filesystem for the partitions we have just created.

    # for the system and the home
    $ mkfs.ext4 /dev/sda1
    $ mkfs.ext4 /dev/sda4

    # for the swap
    $ mkswap /dev/sda3 
    $ swapon /dev/sda3

    # for the EFI
    $ mkfs.vfat -F 32 /dev/sda2

Then we will need to mount the partitions.

    $ mount /dev/sda1 /mnt
    $ mkdir -p /mnt/boot/efi
    $ mount /dev/sda2 /mnt/boot/efi
    $ mkdir /mnt/home
    $ mount /dev/sda4 /mnt/home

### Install the system

This step can be long depending on your internet speed.

    $ pacstrap /mnt base base-devel

### Configure the system

This will save the mount points we have done so that
the file systems are mounted at boot.

    $ genfstab -U /mnt >> /mnt/etc/fstab

Until now the root were the one of the archiso,
we have to change it to make our storage the new root.

    $ arch-chroot /mnt

You may need to install additional packages:

    $ pacman -S linux
    $ pacman -S nano
    $ pacman -S gtk2
    $ pacman -S gtk3
    $ pacman -S openssh
    $ pacman -S efibootmgr
    $ pacman -S grub
    $ pacman -S netcfg
    $ pacman -S dhcpcd

Use:

    $ locale-gen 

to generate a liste of locals in `/etc/locale.gen`
Then edit this file with `nano` or vim:

    $ nano /etc/locale.gen

and <strong>uncomment</strong> the locale you want to use.
I will use `fr_FR.UTF-8` .

    $ echo LANG=fr_FR.UTF-8 > /etc/locale.conf
    $ export LANG=fr_FR.UTF-8

Set the machine name:

    $ echo OnyXstation >/etc/hostname
    $ echo 127.0.0.1 OnyXstation.localdomain OnyXstation >>/etc/hosts

Setup the bootloader:

    $ mkinitcpio -p linux
    $ grub-install /dev/sda
    $ grub-mkconfig -o /boot/grub/grub.cfg

Create a root password:

    $ passwd

finally update the system:

    $ pacman -Syu

reboot the system:

    $ reboot

Once grub appear, select `Reboot into Firmware Interface`.
Then go to `boot manager` and select `arch` since we want 
to boot into our distribution.

Login as `root`, and test internet connection:
    
    $ ping google.com
    
If you have error message such as `cannot resolve hostname xxxxx`:

    $ systemctl enable dhcpcd
    $ systemctl start dhcpcd

Then we will need configure the ssh server we have installed earlier:

    $ echo "PermitRootLogin yes" >>/etc/ssh/sshd_config
    $ systemctl restart sshd

On `virt-manager` go to vm setting tab and select `NIC :xx:xx:xx` on left pan.
then copy the virtual `ip address` of the vm, if it doesn't appear click the `reload` button.

<strong>`Execute these commands on the host`</strong>

Use a hostname for the vm

    $ echo "<the ip address of the vm> OnyXstation" | sudo tee -a /etc/hosts

Use root as default user

    touch  ~/.ssh/config
    $ sudo chmod go-rw ~/.ssh/config

Copy and past this in `~/.ssh/config` :

    Host OnyXstation 
        User root

Copy your public rsa key in the vm's `authorized_keys`

    $ ssh-copy-id OnyXstation

<strong>`Execute these commands on the guest`</strong>

    $ nano /etc/ssh/sshd_config

Uncomment the line `PermitRootLogin prohibit-password` and 
delete the last line we added earlier : `PermitRootLogin yes`.

Shutdown the vm in `virt-manager`

Go to the vm settings, select `Boot options` on left pan and 
disable the `archiso CD`, apply changes and `start` the vm.

## Setup the project workspace

In the <strong>host</strong>:

Download the repository, in a appropriate place.

    # I chosed my home directory
    $ cd ~

    # clone the repository
    $ git clone git@gitlab.com:MatthieuJacquemet/onyx.git

    # Setup the components in the vm (which must be running)
    $ cd onyx
    $ chmod +x install.sh
    $ ./install.sh

Now you can start bringing your modification and
run `install.sh` to push them the the distribution
or write your modification directelly onto the `mnt` folder
which is mounted to the vm's root over sshfs.




